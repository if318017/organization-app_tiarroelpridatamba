import Login from "../../components/Autentication/Login";
import LoginWithGoogle from "../../components/LoginWithGoogle";
// import Logout from "../../components/Logout";
import "../Autentication/LoginPage.css";

function LoginPage() {
  return (
    <div className="page1">
      <h2 style={{ textAlign: "center" }}>Login</h2>
      <Login />
      <LoginWithGoogle />
      {/* <Logout /> */}
    </div>
  );
}

export default LoginPage;
