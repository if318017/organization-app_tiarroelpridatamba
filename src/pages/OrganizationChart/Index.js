import React from "react";
import styled from "@emotion/styled";
import OrganizationChart from "../../components/Chart/OrganizationChart";
import StyleChart from "../../components/Chart/StyleChart";

const Title = styled.h2`
  margin-top: 5rem;
  :first-of-type {
    margin-top: 0;
  }
`;

function Index() {
  return (
    <div style={{ textAlign: "center" }}>
      <Title>Basic tree</Title>
      <OrganizationChart />
      <Title>Styled tree</Title>
      <StyleChart />
    </div>
  );
}

export default Index;
