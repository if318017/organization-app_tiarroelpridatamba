import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import MemberList from "./components/Member/MemberList";
import Register from "./components/Autentication/Register";
import LoginPage from "./pages/Autentication/LoginPage";
import MemberDetail from "./components/Member/MemberDetail";
import CreateMember from "./components/Member/CreateMember";
import OrganizationChart from "./components/Chart/OrganizationChart";

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={ <LoginPage />} />
          <Route path="/member-list" element={<MemberList />} />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/register" element={<Register />} />
          <Route path="/member-detail" element={<MemberDetail />} />
          <Route path="/create-member" element={<CreateMember />} />
          <Route path="/organization-chart" element={<OrganizationChart />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
