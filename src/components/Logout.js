import React from 'react';
import { GoogleLogout } from 'react-google-login';
import swal from "sweetalert";

const clientId =
  '1061948644246-8s2l2f9geml4ocn5lq6dnl56ifh8akn1.apps.googleusercontent.com';

function Logout() {
  const onSuccess = () => {
    console.log('Logout berhasil dilakukan');
    swal({
      title: "Logout Berhasil dilakukan!!",
      button: "Terima Kasih",
    });
  };

  return (
    <div>
      <GoogleLogout
        clientId={clientId}
        buttonText="Logout"
        onLogoutSuccess={onSuccess}
      ></GoogleLogout>
    </div>
  );
}

export default Logout;