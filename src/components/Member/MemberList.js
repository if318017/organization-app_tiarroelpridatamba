import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Container, Table, Button } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faEye } from "@fortawesome/free-solid-svg-icons";

const MemberList = () => {
  return (
    <div>
      <h2 style={{ textAlign: "center", margin: "20px" }}>
        Organizational App
      </h2>
      <Container>
        <h5>Daftar Member</h5>
        <hr />
        <Button className="mb-3 fas fa-plus" variant="info" size="md" href="/create-member">
          <FontAwesomeIcon icon={faPlus} /> Add Member
        </Button>
        <Table striped bordered hover variant="info" style={{textAlign: "center"}}>
          <thead>
            <tr>
              <th>No</th>
              <th>Name</th>
              <th>Position</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>Tiarro</td>
              <td>FrontEnd Developer</td>
              <td><Button href="/member-detail" variant="outline-secondary"><FontAwesomeIcon icon={faEye} /> Detail</Button></td>
            </tr>
          </tbody>
        </Table>
      </Container>
    </div>
  );
};

export default MemberList;
