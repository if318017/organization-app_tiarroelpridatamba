import React from "react";
import { Container, Col } from "react-bootstrap";

const MemberDetail = () => {
  return (
    <div>
      <h4 style={{ textAlign: "center", margin: "20px" }}>Member Data</h4>
      <Container>
        <Col>
          <p>Name : Tiarro</p>
          <p>Position : FrontEnd Developer</p>
          <p>Reports To : Alkahfi</p>
        </Col>
      </Container>
    </div>
  );
};

export default MemberDetail;
