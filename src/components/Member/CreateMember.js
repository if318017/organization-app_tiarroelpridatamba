import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Form, Button, Container } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

const schema = yup
  .object({
    name: yup.string().required(),
    position: yup.string().required(),
    reportsTo: yup.string().required(),
  })
  .required();

const CreateMember = (props) => {
  const navigate = useNavigate();
  const { name, position, reportsTo } = props;

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = (data) => {
    navigate("/member-list");
    localStorage.setItem("name", data.name);
    localStorage.setItem("position", data.position);
    localStorage.setItem("reportsTo", data.reportsTo);
  };

  return (
    <div>
      <h2 style={{ textAlign: "center", margin: "20px" }}>Create Member</h2>
      <Container>
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Form.Group className="mb-3" controlId="name">
            <Form.Label>Name</Form.Label>
            <Form.Control
              type="text"
              {...register("name")}
            />
            <Form.Text className="text-err">
              {errors.name?.message}
            </Form.Text>
          </Form.Group>

          <Form.Group className="mb-3" controlId="position">
            <Form.Label>Position</Form.Label>
            <Form.Control
              type="text"
              {...register("position")}
            />
            <Form.Text className="text-err">
              {errors.position?.message}
            </Form.Text>
          </Form.Group>

          <Form.Group className="mb-3" controlId="reportsTo">
            <Form.Label>Reports To</Form.Label>
            <Form.Control
              type="text"
              {...register("reportsTo")}
            />
            <Form.Text className="text-err">
              {errors.reportsTo?.message}
            </Form.Text>
          </Form.Group>

          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Form>
      </Container>
    </div>
  );
};

export default CreateMember;
