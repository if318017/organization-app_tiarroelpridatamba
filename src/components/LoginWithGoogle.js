import React from "react";
import { GoogleLogin } from "react-google-login";
import { refreshTokenSetup } from "../utils/refreshToken";
import swal from "sweetalert";
import { useNavigate } from "react-router-dom";

const clientId =
  "1061948644246-8s2l2f9geml4ocn5lq6dnl56ifh8akn1.apps.googleusercontent.com";

function LoginWithGoogle() {
  const navigate = useNavigate();
  const onSuccess = (res) => {
    console.log("Berhasil Login: currentUser:", res.profileObj);
    navigate("/member-list");
    swal({
      title: "Berhasil Login!!!",
      text: `Selamat Datang ${res.profileObj.name} di ORGANIZATION APP`,
      icon: "success",
      button: "OK",
    });
    refreshTokenSetup(res);
  };

  const onFailure = (res) => {
    console.log("Gagal Login: res:", res);
    swal({
      title: "Gagal Login!!!",
      timer: 15000,
    });
  };

  return (
    <div>
      <GoogleLogin
        clientId={clientId}
        buttonText="Login"
        onSuccess={onSuccess}
        onFailure={onFailure}
        cookiePolicy={"single_host_origin"}
      />
    </div>
  );
}

export default LoginWithGoogle;
