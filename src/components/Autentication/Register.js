import { useForm } from "react-hook-form";
import "bootstrap/dist/css/bootstrap.min.css";
import { yupResolver } from "@hookform/resolvers/yup";
import { Form, Button } from "react-bootstrap";
import * as yup from "yup";
import { useNavigate } from "react-router-dom";
import '../Autentication/Register.css'

const schema = yup
  .object({
    email: yup.string().required(),
    username: yup.string().required(),
    password: yup.string().required(),
  })
  .required();

export default function Register() {
  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = (data) => {
    navigate("/member-list");
    localStorage.setItem("email", data.email);
    localStorage.setItem("username", data.username);
    localStorage.setItem("password", data.password);
  };

  return (
    <div className="page1"><h2 style={{ textAlign: "center", marginTop: "5px"}}>Register Akun</h2>
    <Form onSubmit={handleSubmit(onSubmit)}>
      <Form.Group className="mt-4 mb-3" controlId="formBasicEmail">
        <Form.Label>Email</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          {...register("email")}
        />
        <Form.Text className="text-err">{errors.email?.message}</Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicUsername">
        <Form.Label>UserName</Form.Label>
        <Form.Control
          type="text"
          placeholder="username"
          {...register("username")}
        />
        <Form.Text className="text-err">{errors.userName?.message}</Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          {...register("password")}
        />
        <Form.Text className="text-err">{errors.password?.message}</Form.Text>
      </Form.Group>

      <p style={{ fontSize: "14px" }}>
        Sudah memiliki Akun? <a href="Login">Login</a>
      </p>

      <Button className="btn-login mb-3" variant="primary" type="submit">
        Register
      </Button>
      </Form>
      </div>
  );
}
